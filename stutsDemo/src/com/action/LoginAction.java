/**
 * 
 */
package com.action;

import com.opensymphony.xwork2.ActionSupport;

/**
 * @author z
 *
 */
public class LoginAction extends ActionSupport
{
	public LoginAction()
	{
		System.out.println("construct LoginAction");
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -8489396337412638935L;

	@Override
	public String execute() throws Exception
	{
		System.out.println("LoginAction execute");
		return SUCCESS;
	}

	public String redirect() throws Exception
	{
		return ERROR;
	}

}
