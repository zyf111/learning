/**
 * 
 */
package com.zyf.learning;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author z
 *
 */
public class TransferBiz implements Runnable
{
	private Bank	   bank;
	private int		   from;
	private static int AMOUNT	= 100;
	private static int INTERVAL	= 10;

	Logger			   logger	= Logger.getGlobal();

	public TransferBiz(Bank bank, int from)
	{
		this.bank = bank;
		this.from = from;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run()
	{
		Thread.currentThread().setUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {

			@Override
			public void uncaughtException(Thread arg0, Throwable arg1)
			{
				logger.log(Level.INFO, arg0.getName() + " had been interrupted by " + arg1.getClass().getName());

			}

		});
		try
		{
			while (true)
			{
				int to = (int) Math.random() * bank.size();
				int amount = 2 * AMOUNT;
				bank.syncTransfer(from, to, amount);

				Thread.sleep((int) Math.random() * INTERVAL);

			}
		}
		catch (InterruptedException e)
		{

		}
	}

}
