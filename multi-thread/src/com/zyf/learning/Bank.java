/**
 * 
 */
package com.zyf.learning;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author z
 *
 */
public class Bank
{
	private static int	  INIT_COUNT	  = 10;
	private static int	  INIT_BALANCE	  = 100;
	private int[]		  accounts		  = new int[INIT_COUNT];

	private ReentrantLock lock			  = new ReentrantLock();
	private Condition	  sufficientFunds = lock.newCondition();

	public Bank()
	{
		for (int i = 0; i < accounts.length; i++)
		{
			accounts[i] = INIT_BALANCE;
		}
	}

	public int size()
	{
		return accounts.length;
	}

	public void print()
	{
		long total = 0;
		for (int i = 0; i < accounts.length; i++)
		{
			total += accounts[i];
		}
		System.out.println("sum : " + total);
	}

	public void transfer(int from, int to, int amount)
	{
		accounts[from] -= amount;
		accounts[to] += amount;
	}

	public void outterSyncTransfer(int from, int to, int amount)
	{
		lock.lock();

		try
		{
			accounts[from] -= amount;
			accounts[to] += amount;
			sufficientFunds.signalAll();
		}
		finally
		{
			lock.unlock();
		}
	}

	public void outterSyncCondTransfer(int from, int to, int amount) throws InterruptedException
	{
		lock.lock();

		try
		{
			while (accounts[from] < amount)
			{
				sufficientFunds.await();
			}
			accounts[from] -= amount;
			accounts[to] += amount;
			sufficientFunds.signalAll();
		}
		finally
		{
			lock.unlock();
		}
	}

	public synchronized void syncTransfer(int from, int to, int amount)
	{
		while (accounts[from] < amount)
		{
			try
			{
				Thread.sleep(50);
			}
			catch (InterruptedException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		accounts[from] -= amount;
		accounts[to] += amount;
	}

	public synchronized void syncCondTransfer(int from, int to, int amount) throws InterruptedException
	{
		while (accounts[from] < amount)
		{
			wait();
		}
		accounts[from] -= amount;
		accounts[to] += amount;
		notifyAll();
	}
}
