/**
 * 
 */
package com.zyf.learning;

import com.zyf.learning.blockingQueue.BlockingQueueTest;

/**
 * @author z
 *
 */
public class Main
{
	private static int TIMES = 10000000;

	public static void main(String args[])
	{
		BlockingQueueTest.test();
	}

	public static void main3(String args[])
	{
		main1(args);
		main1(args);
		main1(args);
	}

	public static void main1(String args[])
	{
		Bank bank = new Bank();

		int from = (int) Math.random() * bank.size();
		int to = (int) Math.random() * bank.size();
		long startTime1 = System.currentTimeMillis();
		for (int i = 0; i < TIMES; i++)
		{

			bank.transfer(from, to, 100);
			bank.transfer(to, from, 100);
		}
		long endTime1 = System.currentTimeMillis();
		System.out.println("time1 " + (endTime1 - startTime1));

		startTime1 = System.currentTimeMillis();
		for (int i = 0; i < TIMES; i++)
		{
			bank.transfer(from, to, 100);
			bank.transfer(to, from, 100);
		}
		endTime1 = System.currentTimeMillis();
		System.out.println("time2 " + (endTime1 - startTime1));

		startTime1 = System.currentTimeMillis();
		for (int i = 0; i < TIMES; i++)
		{
			bank.outterSyncTransfer(1, 2, 100);
			bank.outterSyncTransfer(2, 1, 100);
		}
		endTime1 = System.currentTimeMillis();
		System.out.println("outterSyncTransfer " + (endTime1 - startTime1));

		startTime1 = System.currentTimeMillis();
		for (int i = 0; i < TIMES; i++)
		{
			try
			{

				bank.outterSyncCondTransfer(1, 2, 100);
				bank.outterSyncCondTransfer(2, 1, 100);
			}
			catch (InterruptedException e)
			{

			}
		}

		endTime1 = System.currentTimeMillis();
		System.out.println("outterSyncCondTransfer1 " + (endTime1 - startTime1));

		startTime1 = System.currentTimeMillis();
		try
		{
			for (int i = 0; i < TIMES; i++)
			{
				bank.outterSyncCondTransfer(1, 2, 100);
				bank.outterSyncCondTransfer(2, 1, 100);
			}
		}
		catch (InterruptedException e)
		{

		}
		endTime1 = System.currentTimeMillis();
		System.out.println("outterSyncCondTransfer " + (endTime1 - startTime1));

		startTime1 = System.currentTimeMillis();
		for (int i = 0; i < TIMES; i++)
		{
			bank.syncTransfer(1, 2, 100);
			bank.syncTransfer(2, 1, 100);
		}
		endTime1 = System.currentTimeMillis();
		System.out.println("syncTransfer " + (endTime1 - startTime1));

		startTime1 = System.currentTimeMillis();
		try
		{
			for (int i = 0; i < TIMES; i++)
			{
				bank.syncCondTransfer(1, 2, 100);
				bank.syncCondTransfer(2, 1, 100);
			}
		}
		catch (InterruptedException e)
		{

		}
		endTime1 = System.currentTimeMillis();
		System.out.println("syncCondTransfer " + (endTime1 - startTime1));

		return;
	}

	public static void main4(String args[])
	{
		Bank bank = new Bank();
		TransferBiz[] bizs = new TransferBiz[10];
		Thread[] threads = new Thread[10];
		for (int i = 0; i < 10; i++)
		{
			bizs[i] = new TransferBiz(bank, i);
			threads[i] = new Thread(bizs[i], "thread" + i);
		}

		long startTime1 = System.currentTimeMillis();
		for (int i = 0; i < 10; i++)
		{
			threads[i].start();
		}
		long endTime1 = System.currentTimeMillis();
		while ((endTime1 - startTime1) < 100000)
		{
			try
			{
				Thread.sleep(50);
			}
			catch (InterruptedException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			endTime1 = System.currentTimeMillis();
		}

		for (int i = 0; i < 10; i++)
		{
			// threads[i].interrupt();
		}

		try
		{
			while (true)
			{
				Thread.sleep(50);
			}
		}
		catch (InterruptedException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// bank.print();
	}
}
