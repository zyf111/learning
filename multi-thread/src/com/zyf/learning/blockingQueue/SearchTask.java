/**
 * 
 */
package com.zyf.learning.blockingQueue;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.logging.Logger;

/**
 * @author z
 *
 */
public class SearchTask implements Callable<Integer>
{
	private BlockingQueue<File>	queue;
	private String				key;
	private Logger				logger = Logger.getLogger("blockingQueueLogger");

	/**
	* 
	*/
	public SearchTask(BlockingQueue<File> queue, String key)
	{
		this.queue = queue;
		this.key = key;
	}

	public Integer call()
	{
		int count = 0;
		try
		{
			File file = queue.take();
			if (FileEnumerationTask.isEnd(file))
			{
				queue.put(FileEnumerationTask.DUMMY);
			}
			else
			{
				count = searchAll(file, key);
			}
		}
		catch (InterruptedException e)
		{
			logger.warning("searcher had been interrupted " + e.getMessage());
		}
		catch (FileNotFoundException e)
		{
			logger.warning(e.getMessage());
		}
		return count;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Runnable#run()
	 */

	public void run()
	{
		try
		{
			File file = queue.take();
			if (FileEnumerationTask.isEnd(file))
			{
				queue.put(FileEnumerationTask.DUMMY);
				return;
			}
			else
			{
				search(file, key);
			}
		}
		catch (InterruptedException e)
		{
			logger.warning("searcher had been interrupted " + e.getMessage());
		}
		catch (FileNotFoundException e)
		{
			logger.warning(e.getMessage());
		}
	}

	private int searchAll(File file, String keyStr) throws FileNotFoundException
	{
		int count = 0;
		if (file != null && file.exists())
		{
			if (file.isDirectory())
			{
				File[] files = file.listFiles();
				for (File childFile : files)
				{
					count += searchAll(childFile, keyStr);
				}
			}
			else
			{
				try (Scanner scanner = new Scanner(file))
				{
					while (scanner.hasNextLine())
					{
						String line = scanner.nextLine();
						if (line.contains(keyStr))
						{
							count++;
						}
					}
				}
			}
		}

		return count;
	}

	private void search(File file, String keyStr) throws FileNotFoundException
	{
		try (Scanner scanner = new Scanner(file))
		{
			while (scanner.hasNextLine())
			{
				String line = scanner.nextLine();
				if (line.contains(keyStr))
				{
					logger.info("found " + keyStr + " in " + file.getName() + ": " + line);
					return;
				}
			}
		}

		logger.info(keyStr + " not found in " + file.getName());
	}

}
