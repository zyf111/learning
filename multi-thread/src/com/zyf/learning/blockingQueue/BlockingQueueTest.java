/**
 * 
 */
package com.zyf.learning.blockingQueue;

import java.io.File;
import java.text.MessageFormat;
import java.util.Scanner;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * @author z
 *
 */
public class BlockingQueueTest
{
	public static void test()
	{
		Scanner in = new Scanner(System.in);
		System.out.println("input directory");
		String directory = in.nextLine();

		System.out.println("input key word");
		String key = in.nextLine();

		final int FILE_QUEUE_SIZE = 10;
		final int search_threads = 100;

		// BlockingQueue<File> queue = new
		// ArrayBlockingQueue<File>(FILE_QUEUE_SIZE);
		// new Thread(new FileEnumerationTask(new File(directory),
		// queue)).start();
		File[] files = new File(directory).listFiles();
		int num = 0;
		for (int i = 0; i < files.length; i++)
		{
			System.out.println(files[i].getName());
			if (files[i].isDirectory())
			{
				num++;
			}
		}
		System.out.println(MessageFormat.format("total {0}, directorys: {1}", files.length, num));
		ExecutorService pool = Executors.newCachedThreadPool();

		MatchCounter task = new MatchCounter(new File(directory), key, pool);
		Future<Integer> future = pool.submit(task);

		int count = 0;

		try
		{
			count = future.get();
		}
		catch (InterruptedException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (ExecutionException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(count);
		pool.shutdown();
		int largestPoolSize = ((ThreadPoolExecutor) pool).getLargestPoolSize();
		System.out.println("largestPoolSize: " + largestPoolSize);
	}
}
