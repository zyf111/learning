/**
 * 
 */
package com.zyf.learning.blockingQueue;

import java.io.File;
import java.io.FileNotFoundException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author z
 *
 */
public class MatchCounter implements Callable<Integer>
{
	private File			directory;
	private String			key;
	private ExecutorService	pool;
	private static Logger	logger = Logger.getLogger("MatchCounterLogger");

	/**
	 * 
	 */
	public MatchCounter(File directory, String key, ExecutorService pool)
	{
		this.directory = directory;
		this.key = key;
		this.pool = pool;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.concurrent.Callable#call()
	 */
	@Override
	public Integer call()
	{
		logger.info(
				MessageFormat.format("thread {0} count {1}", Thread.currentThread().getName(), directory.getName()));
		int count = 0;
		if (directory != null && directory.exists() && directory.canRead())
		{
			File[] files = directory.listFiles();
			List<Future<Integer>> futures = new ArrayList<Future<Integer>>();
			for (File file : files)
			{
				if (file.isDirectory())
				{
					MatchCounter task = new MatchCounter(file, key, pool);
					Future<Integer> future = pool.submit(task);
					futures.add(future);
				}
				else
				{
					try
					{
						count += search(file, key);
					}
					catch (FileNotFoundException e)
					{
						logger.log(Level.SEVERE, e.getMessage());
					}
				}
			}

			for (Future<Integer> future : futures)
			{
				try
				{
					count += future.get();
				}
				catch (InterruptedException | ExecutionException e)
				{
					logger.log(Level.SEVERE, e.getMessage());
				}
			}
		}
		return count;
	}

	private int search(File file, String keyStr) throws FileNotFoundException
	{
		int count = 0;
		try (Scanner scanner = new Scanner(file))
		{
			while (scanner.hasNextLine())
			{
				String line = scanner.nextLine();
				if (line.contains(keyStr))
				{
					count++;
				}
			}
		}

		return count;
	}
}
