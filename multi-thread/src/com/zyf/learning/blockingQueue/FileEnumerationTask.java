/**
 * 
 */
package com.zyf.learning.blockingQueue;

import java.io.File;
import java.util.concurrent.BlockingQueue;
import java.util.logging.Logger;

/**
 * @author z
 *
 */
public class FileEnumerationTask implements Runnable
{
	public static File			DUMMY  = new File("");
	private File				startDirectory;
	private BlockingQueue<File>	queue;
	private Logger				logger = Logger.getLogger("blockingQueueLogger");

	public FileEnumerationTask(File directory, BlockingQueue<File> queue)
	{
		this.startDirectory = directory;
		this.queue = queue;
	}

	@Override
	public void run()
	{
		try
		{
			enumerate(startDirectory, queue);
			queue.put(DUMMY);
		}
		catch (InterruptedException e)
		{
			logger.warning("enumerater had been interrupted " + e.getMessage());
		}
	}

	public static boolean isEnd(File file)
	{
		if (file == DUMMY)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	private void enumerate(File directory, BlockingQueue<File> queue) throws InterruptedException
	{
		File[] files = directory.listFiles();
		for (File file : files)
		{
			queue.put(file);
		}
	}
}
