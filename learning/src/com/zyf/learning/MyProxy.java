/**
 * 
 */
package com.zyf.learning;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;
import java.util.Arrays;
import java.util.Random;

/**
 * @author z
 *
 */
public class MyProxy
{
	public static void test()
	{

		Object[] elements = new Object[1000];

		Class[] interfaces = new Class[] { Comparable.class };
		for (int i = 0; i < elements.length; i++)
		{
			Integer value = i + 1;
			InvocationHandler handler = new TraceHandler(value);
			elements[i] = Proxy.newProxyInstance(null, interfaces, handler);
		}

		Integer key = new Random().nextInt(elements.length) + 1;

		int result = Arrays.binarySearch(elements, key);

		if (result >= 0)
		{
			System.out.println(elements[result]);
		}
	}
}
