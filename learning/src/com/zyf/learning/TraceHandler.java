/**
 * 
 */
package com.zyf.learning;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.text.MessageFormat;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author z
 *
 */
public class TraceHandler implements InvocationHandler
{
	private Object target;
	Logger		   logger = Logger.getGlobal();

	public TraceHandler(Object t)
	{
		target = t;
	}

	public Object invoke(Object proxy, Method m, Object[] args) throws Throwable
	{
		logger.entering("com.zyf.learning.TraceHandler", "in");
		// Object[] arg = { target, m.getName() };
		logger.log(Level.INFO, MessageFormat.format("{0}.{1}", target, m.getName()));
		// System.out.print(target + ".");
		// System.out.print(m.getName());

		if (args != null)
		{
			logger.log(Level.INFO, Arrays.toString(args));
			// System.out.println(Arrays.toString(args));
		}

		return m.invoke(target, args);
	}
}
